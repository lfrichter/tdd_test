<?php

namespace Tests\Feature;

use App\Models\Checkout;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CalculationTest extends TestCase
{
    /** @test */
    public function check_an_apple_price()
    {
        $checkout = new Checkout;
        $price = $checkout->scan("A99");

        $this->assertEquals(50, $price);
    }

    /** @test */
    public function check_two_apples_price()
    {
        $checkout = new Checkout;
        $price = $checkout->scan("A99",2);

        $this->assertEquals(100, $price);
    }

    /** @test */
    public function check_three_apples_price()
    {
        $checkout = new Checkout;
        $price = $checkout->scan("A99",3);

        $this->assertEquals(130, $price);
    }

    /** @test */
    public function check_item_b15_price()
    {
        $checkout = new Checkout;
        $price = $checkout->scan("B15");

        $this->assertEquals(30, $price);

    }

    /** @test */
    public function check_two_b15_price()
    {
        $checkout = new Checkout;
        $price = $checkout->scan("B15",2);

        $this->assertEquals(45, $price);

    }

}
