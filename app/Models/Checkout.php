<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{

    public function scan(string $item, int $quantity = 1)
    {
        $price = $offer = $offer_price = 0;

        switch ($item){
            case 'A99':
                $price = 50;
                $offer = 3;
                $offer_price = 130;
            break;
            case 'B15':
                $price = 30;
                $offer = 2;
                $offer_price = 45;
            break;
        }

        if($offer === $quantity){
            return $offer_price;
        }

        return $price*$quantity;
    }

}
